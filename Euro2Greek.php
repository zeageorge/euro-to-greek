<?php

namespace zeageorge\euro2greek;

/**
 * Description of Euro2Greek
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class Euro2Greek {

  /**
   * 
   * @param float $amount format 123456.75
   * @param boolean $parse_zero 
   * @param string $currency
   * @param string $glue
   * @return string
   */
  public static function translate($amount, $parse_zero = true, $currency = 'ευρώ', $glue = 'και') {
    if (!is_numeric($amount)) {
      return '';
    }

    $curren = [0 => $currency, 'λεπτά', 'λεπτό'];
    list($integer, $fraction) = explode('.', number_format(round($amount, 2), 2, '.', ''));

    if ($integer == 0 && $fraction == 0) {
      return $parse_zero ? "μηδέν {$curren[0]}" : '';
    }

    $sentence = '';
    $integer_part_sentence = '';
    $fraction_part_sentence = '';

    if ($integer[0] == '-' && ((int) substr($integer, 1) > 0 || (int) $fraction > 0)) {
      $sentence .= 'μείον ';
      $integer = ltrim($integer, '-');
    }

    $fraction_part_sentence .= self::integer_to_words((int) $fraction);
    if ($amount == .01 || $amount == -.01) {
      return "{$sentence}{$fraction_part_sentence}{$curren[2]}";
    }

    $integer_part_sentence .= self::integer_to_words((int) $integer);

    $sentence .= $integer_part_sentence != '' ? "{$integer_part_sentence}{$curren[0]}" : '';
    if ($fraction_part_sentence != '' && $integer_part_sentence != '') {
      $sentence .= " {$glue} {$fraction_part_sentence}{$curren[1]}";
    } else if ($fraction_part_sentence != '' && $integer_part_sentence == '') {
      $sentence .= "{$fraction_part_sentence}{$curren[1]}";
    }
    return $sentence;
  }

  public static function integer_to_words($integer) {
    if (!is_int($integer)) {
      return '';
    }
    $triades = [
      1 => '',
      1000 => 'χιλιάδες ',
      1000000 => 'εκατομμύρια ',
      1000000000 => 'δισεκατομμύρια ',
      1000000000000 => 'τρισεκατομμύρια ',
    ];
    $deca = [
      1 => '',
      1000 => 'χίλια ',
      1000000 => 'εκατομμύριο ',
      1000000000 => 'δισεκατομμύριο ',
      1000000000000 => 'τρισεκατομμύριο ',
    ];

    if ($integer > 999 && $integer < 2000) {
      return $deca[1000] . self::three_digits_integer_to_words($integer - 1000);
    }

    $integer_part_sentence = '';
    $groups = explode(',', number_format($integer));
    $groups_len = count($groups);
    for ($i = 0; $i < $groups_len; $i++) {
      $n = (int) $groups[$i];
      if ($n == 0) {
        continue;
      }
      $exp2 = pow(10, 3 * ($groups_len - ($i + 1)));
      if ($n == 1) {
        $integer_part_sentence .= ($i != $groups_len - 2) ?
          self::three_digits_integer_to_words($n) . $deca[$exp2] :
          $deca[$exp2];
        continue;
      }
      $integer_part_sentence .= self::three_digits_integer_to_words($n, ($i == $groups_len - 2)) . $triades[$exp2];
    }
    return $integer_part_sentence;
  }

  public static function three_digits_integer_to_words($three_digits_integer, $th = false) {
    if (!is_int($three_digits_integer) || strlen((string) $three_digits_integer) > 3) {
      return '';
    }
    $decas = [
      1 => 'μια ',
      3 => 'τρεις ',
      4 => 'τέσσερεις ',
      100 => 'εκατόν ',
      200 => 'διακόσιες ',
      300 => 'τριακόσιες ',
      400 => 'τετρακόσιες ',
      500 => 'πεντακόσιες ',
      600 => 'εξακόσιες ',
      700 => 'επτακόσιες ',
      800 => 'οκτακόσιες ',
      900 => 'εννιακόσιες ',
    ];
    $deca = [
      0 => '',
      1 => 'ένα ',
      2 => 'δύο ',
      3 => 'τρία ',
      4 => 'τέσσερα ',
      5 => 'πέντε ',
      6 => 'έξι ',
      7 => 'επτά ',
      8 => 'οκτώ ',
      9 => 'εννέα ',
      10 => 'δέκα ',
      11 => 'ένδεκα ',
      12 => 'δώδεκα ',
      20 => 'είκοσι ',
      30 => 'τριάντα ',
      40 => 'σαράντα ',
      50 => 'πενήντα ',
      60 => 'εξήντα ',
      70 => 'εβδομήντα ',
      80 => 'ογδόντα ',
      90 => 'ενενήντα ',
      100 => 'εκατό ',
      200 => 'διακόσια ',
      300 => 'τριακόσια ',
      400 => 'τετρακόσια ',
      500 => 'πεντακόσια ',
      600 => 'εξακόσια ',
      700 => 'επτακόσια ',
      800 => 'οκτακόσια ',
      900 => 'εννιακόσια ',
    ];
    $n = $three_digits_integer;
    if (isset($deca[$n])) {
      return $th && isset($decas[$n]) && $n != 100 ? $decas[$n] : $deca[$n];
    }
    $integer_part_sentence = '';
    while ($n > 0) {
      if ($n == 11 || $n == 12) {
        $integer_part_sentence .= $deca[$n];
        break;
      }
      $exp = pow(10, strlen((string) $n) - 1) * (int) substr((string) $n, 0, 1);
      $n -= $exp;
      if ($exp == 100) {
        $integer_part_sentence .= $decas[$exp];
        continue;
      }
      $integer_part_sentence .= $th && isset($decas[$exp]) ? $decas[$exp] : $deca[$exp];
    }
    return $integer_part_sentence;
  }

}
